<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rangga aditya permana</title>
    <link rel="stylesheet" href="home.css">
    
</head>
<body>
    <header>
        <h1>Rangga aditya permana</h1>
        <nav>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="#">Portfolio</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
        </nav>
    </header>

    <main>
        <div class="container">
            <div class="profile">
                <img src="rangga.jpg" alt="Profile Picture">
            </div>
            <div>
                <h2 style="text-align: right; font-size: 32px;">Nama:Rangga aditya permana</h2> 
                <h2 style="text-align: right; font-size: 32px;">NIM:2200018308</h2>
                <h3 style="text-align: right; font-size: 32px;">Kelas:G</h3>
                <h4 style="text-align: right; font-size: 24px;">Web Developer</h4>
            </div>
        </div>
    </main>

    <footer></footer>
</body>
</html>
